import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
from pathlib import Path
import itertools
import pickle
import seaborn

seaborn.set()

import searchengine as se
from pathlib import Path


def compute_and_serialize_kp_and_des(dataset):
    """ Compute keypoints and descriptors for every jpg picture in dataset_path
    and save them serialized.    
    """

    sift = cv.xfeatures2d.SIFT_create(contrastThreshold = 0.05 , edgeThreshold = 5)

    results = {}
    for i, jpg in enumerate(Path(dataset).glob("**/*.jpg")):
        jpg = str(jpg)
        img = cv.imread(jpg,1)
        try:
            kp, des = sift.detectAndCompute(img,None)
        except:
            print(jpg)
            raise
        serialized_kp = []
        for point in kp:
            temp = (point.pt, point.size, point.angle, point.response, point.octave, point.class_id)
            serialized_kp.append(temp)

        results[jpg.replace("\\","/")] = [serialized_kp,des]



    with open('kp_des_' + dataset + '.p', 'wb') as fp:
        pickle.dump(results, fp)

def deserialize_kp_and_des(serialized_kp_path):
    """ Load a image database serialized with "compute_and_serialize_kp_and_des()"
    """
    with open(serialized_kp_path, 'rb') as fp:
        serialized_kp = pickle.load(fp)

    results = {}
    for jpg in serialized_kp:
        serialized_kp_list = serialized_kp[jpg][0]
        kp_list = [0 for x in range(len(serialized_kp_list))]
        for i, kp in enumerate(serialized_kp_list):
            kp_list[i] = cv.KeyPoint(x=kp[0][0], y=kp[0][1], _size=kp[1], _angle=kp[2],
                         _response=kp[3], _octave=kp[4], _class_id=kp[5])
        results[jpg] = [kp_list,serialized_kp[jpg][1]]
    return results

def rank(query_img, database, verbose = False):
    query_img = str(Path(query_img))
    query = cv.imread(query_img,1)
    sift = cv.xfeatures2d.SIFT_create(contrastThreshold = 0.05 , edgeThreshold = 5)

    query_kp , query_des = sift.detectAndCompute(query,None)

    bf = cv.BFMatcher()
    distances = {}
    for i, jpg in enumerate(database):
        try:   
            matches = bf.knnMatch(query_des ,database[jpg][1],k=2)
            matches = sorted(matches,  key = lambda x :x[0].distance)
            good = []
            db_img_kps = []
            for m,n in matches:
                if m.distance < 0.75*n.distance:
                    if m.trainIdx not in db_img_kps:
                        good.append(m)
                        db_img_kps.append(m.trainIdx)
                        if len(good) > 0:
                            avg = sum([match.distance for match in good]) / len(good)
                            distances[jpg] = {"avg_distance":avg, "matches": matches, "good": good, 
                                              "dist1": avg, "dist2": avg*((len(matches)-len(good))**2), 
                                              "dist3": avg*((len(matches)-len(good)**2)),  
                                              "dist4": avg*((len(matches)-len(good))**2)/len(good)}
        except:
            if verbose:
                print(jpg)
                
    rankings = []
    rankings.append( sorted(distances, key= lambda x:distances[x]["dist1"]))
    rankings.append( sorted(distances, key= lambda x:distances[x]["dist2"]))
    rankings.append( sorted(distances, key= lambda x:distances[x]["dist3"]))
    rankings.append( sorted(distances, key= lambda x:distances[x]["dist4"]))
    
    return rankings, distances

def visualize_ranking(query_img, ranking, distances, with_distance = True, num_columns = 3, num_results = 40, width = 20, height = 50):
    j = 0
    num_img_per_batch = num_columns * 3
    query_img = str(Path(query_img))
    
    plt.figure(figsize=(width,height))
    for i,jpg in enumerate([query_img] + ranking[0:num_results+1]):
        if(i != 0 and i % num_img_per_batch == 0):
            plt.show()
            plt.figure(figsize=(width,height))
            j += 1
        if(i == num_results):
            break
        jpg2 =  str(Path(jpg))
        img = cv.imread(jpg2,1)
        b,g,r = cv.split(img)
        img = cv.merge([r,g,b])

        plt.subplot(5,num_columns,i+1-j*num_img_per_batch);

        #plt.title("Ranking {}, Relevant {}".format(i+1,is_relevant(key,jpg)))
        if i != 0:
            if with_distance:
                plt.title("Ranking {} with Dist {}".format(i,distances[jpg]["dist4"]), fontsize=20)
            else:
                plt.title("Ranking {}".format(i), fontsize=20)
        else:
            plt.title("Query image", fontsize=20)
        plt.xticks([]), plt.yticks([])
        plt.imshow(img)
    plt.show()
    plt.clf()
    
def draw_matches(query_img, db_img, filtered = True, verbose = False):
    query_img = str(Path(query_img))
    db_img = str(Path(db_img))
    sift = cv.xfeatures2d.SIFT_create(contrastThreshold = 0.05 , edgeThreshold = 5)
    query = cv.imread(query_img,1)
    db_img = cv.imread(db_img,1)
    
    query_kp , query_des = sift.detectAndCompute(query,None)
    db_img_kp , db_img_des = sift.detectAndCompute(db_img,None)
    
    #filtered switches between showing all matches or only filtered matches of the advanced ratio test with only one match per kp
    bf = cv.BFMatcher()
    if filtered:
        matches = sorted(bf.knnMatch(query_des ,db_img_des,k=2),  key = lambda x :x[0].distance)
        good = []
        db_img_kps = []
        for m,n in matches:
            if m.distance < 0.75*n.distance:
                if m.trainIdx not in db_img_kps:
                    good.append(m)
                    db_img_kps.append(m.trainIdx)
                    
        matches_img = cv.drawMatches(query, query_kp, db_img, db_img_kp, good, None, flags=4)
    else:
        matches = bf.match(query_des ,db_img_des)
        matches_img = cv.drawMatches(query, query_kp, db_img, db_img_kp, matches, None, flags=4)
    b,g,r = cv.split(matches_img)
    matches_img = cv.merge([r,g,b])
    plt.figure(figsize=(20,20))
    plt.xticks([]), plt.yticks([])
    plt.imshow(matches_img)
    plt.show()
    if verbose:
        print(len(matches))
        if filtered:
            print(len(good))
            avg = sum([match.distance for match in good]) / len(good)
            print(avg)
            print(avg*(len(matches)-len(good)))
            print(sum(sorted([match.distance for match in good])[:10])/len(sorted([match.distance for match in good])[:10]))
            print(avg*((len(matches)-len(good)**2))/len(good))
        else:
            print(sum([match.distance for match in matches]) / len(matches))
        
def draw_img(img, with_kp = True):
    img = str(Path(img))
    sift = cv.xfeatures2d.SIFT_create(contrastThreshold = 0.05 , edgeThreshold = 5)
    img = cv.imread(img,1)
    kp , des = sift.detectAndCompute(img,None)
    
    if with_kp:
        img = cv.drawKeypoints(img,kp,None)
    else:
        img = img
    b,g,r = cv.split(img)
    img = cv.merge([r,g,b])
    plt.figure(figsize=(10,10))
    plt.xticks([]), plt.yticks([])
    plt.imshow(img)
    plt.show()
    
  
