# /* ------------------ IMPORTS --------------------*/
# /* -----------------------------------------------*/
import numpy as np
import cv2 as cv
from pathlib import Path
import itertools
import pickle

import os
import io
from PIL import Image
from array import array

import mysql.connector as ms

db = ms.connect(
	host = "localhost",
	user = "root",
	passwd = " ",
	database = "searchengine_hm"
)

cursor = db.cursor()

def check_if_table_exists():
	cursor.execute("SHOW TABLES LIKE 't_shirts'")
	#~ print(cursor.rowcount)
	#~ print(len(cursor.fetchall()))
	output = cursor.fetchall()
	if len(output) == 0:
		return False
	else:
		return True

def create_t_shirt_table():
	if not check_if_table_exists():
		cursor.execute("CREATE TABLE t_shirts( \
			t_shirt_id VARCHAR(255) NOT NULL, \
			point_x FLOAT NOT NULL, \
			point_y FLOAT NOT NULL, \
			size FLOAT NOT NULL, \
			angle FLOAT NOT NULL, \
			response FLOAT NOT NULL, \
			octave INT NOT NULL, \
			class_id INT NOT NULL, \
			PRIMARY KEY (t_shirt_id) \
		)")
	else:
		print('Table t_shirts already exists!')

def compute_and_serialize_kp_and_des(dataset):
    """ Compute keypoints and descriptors for every jpg picture in dataset_path
    and save them serialized.    
    """

    sift = cv.xfeatures2d.SIFT_create(contrastThreshold = 0.05 , edgeThreshold = 5)

    results = {}
    for i, jpg in enumerate(Path(dataset).glob("**/*.jpg")):
    #~ for i, jpg in enumerate(Path(dataset).glob("**/*")):
        jpg = str(jpg)
        img = cv.imread(jpg,1)
        try:
            kp, des = sift.detectAndCompute(img,None)
        except:
            print(jpg)
            raise
        serialized_kp = []
        for point in kp:
            temp = (point.pt, point.size, point.angle, point.response, point.octave, point.class_id)
            serialized_kp.append(temp)

        results[jpg.replace("\\","/")] = [serialized_kp,des]



    with open('kp_des_' + dataset + '.p', 'wb') as fp:
        pickle.dump(results, fp)

def deserialize_kp_and_des(serialized_kp_path):
    """ Load a image database serialized with "compute_and_serialize_kp_and_des()"
    """
    with open(serialized_kp_path, 'rb') as fp:
        serialized_kp = pickle.load(fp)

    results = {}
    for jpg in serialized_kp:
        serialized_kp_list = serialized_kp[jpg][0]
        kp_list = [0 for x in range(len(serialized_kp_list))]
        for i, kp in enumerate(serialized_kp_list):
            kp_list[i] = cv.KeyPoint(x=kp[0][0], y=kp[0][1], _size=kp[1], _angle=kp[2],
                         _response=kp[3], _octave=kp[4], _class_id=kp[5])
        results[jpg] = [kp_list,serialized_kp[jpg][1]]
    return results

def rank(query_path, database, verbose = False):
	sift = cv.xfeatures2d.SIFT_create(contrastThreshold = 0.05 , edgeThreshold = 5)
	
	query_path = str(Path(query_path))
	
	#query = np.asarray(test_image)
	query_img = cv.imread(query_path,1)
	query_kp , query_des = sift.detectAndCompute(query_img,None)

	bf = cv.BFMatcher()
	distances = {}
	
	jpg_index = {}
	
	with open("edge_list.dat", "w+") as results_file:
		for jpgCOUNT, jpgTEST in enumerate(database):
			print(jpgTEST)
			jpg_index[jpgTEST] = jpgCOUNT
			
		for jpgCOUNT, jpgTEST in enumerate(jpg_index):
			query_path = str(Path(jpgTEST))
			query_img = cv.imread(query_path,1)
			query_kp , query_des = sift.detectAndCompute(query_img,None)

			bf = cv.BFMatcher()
			distances = {}
			
			for i, jpg in enumerate(database):
				if jpg == jpgTEST:
					continue
				try:   
					matches = bf.knnMatch(query_des ,database[jpg][1],k=2)
					matches = sorted(matches,  key = lambda x :x[0].distance)
					good = []
					db_img_kps = []
					for m,n in matches:
						if m.distance < 0.75*n.distance:
							if m.trainIdx not in db_img_kps:
								good.append(m)
								db_img_kps.append(m.trainIdx)
								if len(good) > 0:
									avg = sum([match.distance for match in good]) / len(good)
									distances[jpg] = {"avg_distance":avg, "matches": matches, "good": good, 
													  "dist1": avg, "dist2": avg*((len(matches)-len(good))**2), 
													  "dist3": avg*((len(matches)-len(good)**2)),  
													  "dist4": avg*((len(matches)-len(good))**2)/len(good)}
				except:
					if verbose:
						print(jpg)
					
			rankings = []
			rankings.append( sorted(distances, key= lambda x:distances[x]["dist1"]))
			rankings.append( sorted(distances, key= lambda x:distances[x]["dist2"]))
			rankings.append( sorted(distances, key= lambda x:distances[x]["dist3"]))
			rankings.append( sorted(distances, key= lambda x:distances[x]["dist4"]))
			
			jpgT0 = str(jpgCOUNT)
			jpgT1 = str(jpgTEST).split("/")[1].replace(".jpg","")
			rankT0 = jpg_index[ str(rankings[3][0]) ]
			rankT1 = str(rankings[3][0]).split("/")[1].replace(".jpg","")
			
			results_file.write("%s\t%s\t%s\t%s\n" % (jpgT0, rankT0, jpgT1, rankT1))
			print("%s\t%s\t%s\t%s\n" % (jpgT0, rankT0, jpgT1, rankT1))
			
		print(jpgCOUNT)

	return rankings, distances

    
  
