import searchengine_no_drawings_dev as se
from pathlib import Path
import sys
import os

#Database creation can take some minutes.
#~ if not Path("kp_des_dataset_zalando.p").exists():
    #~ se.compute_and_serialize_kp_and_des("dataset_zalando")
    
store="hm_medium"
if not Path("kp_des_dataset_"+ store + ".p").exists():
    se.compute_and_serialize_kp_and_des("dataset_"+ store)
    
database = se.deserialize_kp_and_des("kp_des_dataset_"+ store + ".p")

query_img = sys.argv[1]
response_file = sys.argv[2]
rankings, distances = se.rank(query_img,database)

show_rankings = rankings[3]

products = [];
result_count = 0

with open(response_file, "w+") as results_file:
	for item in show_rankings:
		item = item.split("/")[0]
		if ".jpg" in item:
			item = item.replace(".jpg","")
		item_array = item.split("_")[:-1]
		product_id = ""
		for elem in item_array:
			product_id += elem + "_"
		product_id += "PLACEHOLDER_TEMP"
		product_id = product_id.replace("_PLACEHOLDER_TEMP", "")
		if product_id not in products:
			products.append(product_id)
			#~ print("%s" % product_id)
			results_file.write("%s\n" % product_id)
			result_count +=1
		if result_count>2:
			break
		
	
